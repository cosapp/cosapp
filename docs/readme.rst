.. image:: ./img/gitlab-cosapp-f4950f.svg
    :alt: gitlab
    :target: https://gitlab.com/cosapp/cosapp
.. image:: https://joss.theoj.org/papers/10.21105/joss.06292/status.svg
   :target: https://doi.org/10.21105/joss.06292

.. mdinclude:: ../README.md
