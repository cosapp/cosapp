{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Design Methods\n",
    "\n",
    "Design methods allow component designers to identify, from expert knowledge, the different ways users can design a component from functional requirements. \n",
    "\n",
    "### Declaring a design method in a system\n",
    "\n",
    "Design methods are declared at `System` setup, using `System.add_design_method`.\n",
    "This class method takes the name of the design method as single argument; it will create a new entry in an internal dictionary of `MathematicalProblem` objects, mapped to their names.\n",
    "\n",
    "Such objects bear unknowns and equations, declared with methods `add_unkown` and `add_equation`:\n",
    "\n",
    "```python\n",
    "class MySystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "        design = self.add_design_method('find_root')    # create problem `design`, and store it with key 'find_root'\n",
    "        design.add_unknown('x').add_equation('y == 0')  # define problem by declaring unknowns and equations\n",
    "\n",
    "    def compute(self):\n",
    "        self.y = self.x**2 - 3\n",
    "```\n",
    "\n",
    "In practice, design methods are mathematical problems that can be activated on demand."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System, Port\n",
    "\n",
    "\n",
    "class XPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('x', 1.0)\n",
    "\n",
    "\n",
    "class MultiplyWithDesignMethod(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in', {'x': 1.0})\n",
    "        self.add_output(XPort, 'p_out', {'x': 1.0})\n",
    "\n",
    "        self.add_inward('K1', 5.0)\n",
    "        \n",
    "        # intrinsic constraints\n",
    "        self.add_inward('expected_output', 1.0)\n",
    "        self.add_unknown('p_in.x').add_equation('p_out.x == expected_output')\n",
    "\n",
    "        # design methods\n",
    "        self.add_design_method('dx').add_unknown('K1').add_equation('p_out.x - p_in.x == 5') \n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.x = self.p_in.x * self.K1\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Class `MultiplyWithDesignMethod` defines two types of mathematical problems, through `add_unknown` and `add_equation`:\n",
    "\n",
    "1. Unknowns and equations declared directly in the system definition (that is `self.add_unknown` and `self.add_equation` in system `setup`) are always enforced, for all instances of the class. They are referred to as the **intrinsic problem** of the class. Composite systems automatically collect the intrinsic problems of their sub-systems.\n",
    "\n",
    "2. Unknowns and equations declared within a design method define a **design problem**, which may or may not be activated.\n",
    "\n",
    "### Solving the intrinsic problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "# Create system\n",
    "m = MultiplyWithDesignMethod('m')\n",
    "\n",
    "# Add solver\n",
    "solver = m.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "# Initialize & solve system\n",
    "m.K1 = 5.0\n",
    "m.expected_output = 7.5\n",
    "m.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"Intrinsic problem for K1 = {m.K1}:\",\n",
    "    solver.problem,\n",
    "    \"\",\n",
    "    \"Result:\",\n",
    "    f\"{  m.K1 = }\",\n",
    "    f\"{  m.p_in.x = }\",\n",
    "    f\"{  m.p_out.x = }\",\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Activating a design method\n",
    "\n",
    "Design methods are activated by *extending* an existing mathematical problem with the predefined design method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "m = MultiplyWithDesignMethod('m')\n",
    "\n",
    "solver = m.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "# Activate design method 'dx' of system `m`\n",
    "solver.extend(m.design_methods['dx'])\n",
    "\n",
    "# Initialize & solve system\n",
    "m.K1 = 5.0\n",
    "m.expected_output = 7.5\n",
    "m.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Design problem:\",\n",
    "    solver.problem,\n",
    "    \"\",\n",
    "    \"Result:\",\n",
    "    f\"  {m.K1 = }\",\n",
    "    f\"  {m.p_in.x = }\",\n",
    "    f\"  {m.p_out.x = }\",\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reusing sub-system design methods at parent level\n",
    "\n",
    "Composite systems can take advantage of design methods defined for their sub-systems, and thus construct composite design methods.\n",
    "\n",
    "### Aggregation of design problems by extension\n",
    "\n",
    "In the example below, design method \"design\" merges design constraints from sub-systems `a` and `b`:\n",
    "\n",
    "```python\n",
    "class CompositeSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        a = self.add_child(ComponentA('a'))\n",
    "        b = self.add_child(ComponentB('b'))\n",
    "\n",
    "        design = self.add_design_method('design')\n",
    "        design.extend(a.design_methods['design_this'])\n",
    "        design.extend(b.design_methods['design_that'])\n",
    "```\n",
    "\n",
    "### Promotion of sub-system design methods\n",
    "\n",
    "One can also use method `System.pull_design_method` to promote sub-system design methods at parent level.\n",
    "Arguments are (1) the sub-system (or list thereof) from which design methods are promoted, and (2) the list of design method names, or a name mapping, if required (rules for variable pulling and connections apply - see tutorial on [Systems](01-Systems.ipynb)).\n",
    "\n",
    "In the example below, design method \"design_x\", merging design problems from sub-systems `a` and `b`, is created at parent level.\n",
    "Additionaly, design method \"design_y\" of sub-system `a` is exposed at parent level as \"design_foo\".\n",
    "\n",
    "```python\n",
    "class CompositeSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        a = self.add_child(ComponentA('a'))\n",
    "        b = self.add_child(ComponentB('b'))\n",
    "\n",
    "        self.pull_design_method([a, b], 'design_x')\n",
    "        self.pull_design_method(a, {'design_y': 'design_foo'})\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Write dynamic design problems as system methods\n",
    "\n",
    "As mentioned earlier, a design method is a predefined mathematical problem, stored in dictionary `design_methods`, to be used in design problems.\n",
    "The main limitation is that design methods defined at `setup` are static, that is defined once and for all.\n",
    "\n",
    "For advanced uses, one may want to create mathematical problems dynamically, with optional parameters, say.\n",
    "This can typically be achieved by writing dedicated object-bound methods creating said problem on-the-fly, using custom arguments.\n",
    "A convenient function, `System.new_problem`, may be used to this end:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "from cosapp.base import System\n",
    "from cosapp.core import MathematicalProblem\n",
    "\n",
    "\n",
    "class SystemWithDynamicDesignMethod(System):\n",
    "\n",
    "    def setup(self):\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "    def compute(self):\n",
    "        self.y = math.sin(self.x**2 - 2)\n",
    "    \n",
    "    def find_root(self, **options) -> MathematicalProblem:\n",
    "        \"\"\"Compute `x` such that `y == 0`.\n",
    "        Additional options apply to unknown `x`.\n",
    "        \"\"\"\n",
    "        problem = self.new_problem()\n",
    "        problem.add_unknown('x', **options)\n",
    "        problem.add_equation('y == 0')\n",
    "        return problem\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First simulation, with no constraint on unknown `x`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "s = SystemWithDynamicDesignMethod('s')\n",
    "\n",
    "solver = s.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "solver.extend(s.find_root())\n",
    "\n",
    "s.x = 0.7  # initial value\n",
    "s.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"{s.x = }\",\n",
    "    f\"{s.y = }\",\n",
    "    f\"Converged in {solver.results.fres_calls} iterations\",\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second try, imposing a maximum step on `x` at each solver iteration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = SystemWithDynamicDesignMethod('s')\n",
    "\n",
    "solver = s.add_driver(NonLinearSolver('solver'))\n",
    "\n",
    "solver.extend(s.find_root(max_abs_step=0.1))\n",
    "\n",
    "s.x = 0.7  # initial value\n",
    "s.run_drivers()\n",
    "\n",
    "print(\n",
    "    f\"{s.x = }\",\n",
    "    f\"{s.y = }\",\n",
    "    f\"Converged in {solver.results.fres_calls} iterations\",\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Show information on design method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(s.find_root)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "**Congrats!** You are now ready to update your `System` into a design model with **CoSApp**!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "cosapp",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
